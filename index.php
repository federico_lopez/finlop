<?php
//https://github.com/codeguy/Slim-Views

session_start();
require 'vendor/autoload.php';
$app = new \Slim\Slim(array(
	//para cargar el twig
    'view' => new \Slim\Views\Twig()
));

// configuraciones de slim
$app->config(array(
	'debug' => true,
	'templates.path' => 'views'
));

//configuraiciones de twig
$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);

//carga de extenciones de twig ---urlFor siteUrl baseUrl
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

//Hooks
$app->hook('slim.before', function() use($app){
	$app->view()->appendData(array('baseUrl' => WEB));
});

define('SPECIALCONSTANT', true);
define('WEB', 'http://192.168.0.16/finlop');
//define('WEB', 'http://localhost/finlop');


//Conexion
require_once 'conexion.php';
require_once 'controllers/login.php';
require_once 'controllers/logout.php';
require_once 'controllers/dashboard.php';
require_once 'controllers/categoria.php';
require_once 'controllers/cliente.php';
require_once 'controllers/activo.php';
require_once 'controllers/pasivo.php';
require_once 'controllers/deudas.php';


//Index
$app->get('/',function() use($app){
	$app->redirect('login');
})->name('index');



/*$app->get('/{name}', function ($request, $response, $args) {
	return $this->view->render($response, 'profile.html.twig', [
			'name' => $args['name']
	]);
})->setName('profile');*/

$app->run();

?>