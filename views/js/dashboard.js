$(function(){
    $('.date-picker').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
});


$('#moneda').keyup(function () {
    var strn = $(this).val();
    if (strn != this.value.replace(/[^0-9\.]/g, '')) {
       this.value = this.value.replace(/[^0-9\.]/g, '');
    }else{
        var nocero = strn.replace(/\./g, '');
        var num = parseInt(nocero);
        $(this).val(num.toLocaleString('es-PY'));
        //alert(strn);
    }
});

$('#guardar_salir').click(function(){
    $('#cerrar_modal').click();
    location.reload();
});


$(function(){
    $(".navbar-expand-toggle").click(function() {
        $(".app-container").toggleClass("expanded");
        return $(".navbar-expand-toggle").toggleClass("fa-rotate-90");
    });
    return $(".navbar-right-expand-toggle").click(function() {
        $(".navbar-right").toggleClass("expanded");
        return $(".navbar-right-expand-toggle").toggleClass("fa-rotate-90");
    });
});

//insertar registros
$(function(){
    $('form[name="insertar"]').on('submit', function(e){
        e.preventDefault();
        var pet = $(this).attr('action');
        var met = $(this).attr('method');

        $.ajax({
            beforeSend: function(){
                $('#guardar').attr({'value':'Enviando...', 'disabled':'disabled'}).css({'background-color':'#fff', 'border-color':'#1b9b77', 'color':'#1b9b77'});
            },
            url : pet,
            type: met,
            data: $(this).serialize(),
            success: function(resp){
                if(resp == "true"){
                    //guardado();
                    $('.clear').val('');
                    $('#guardar').attr({'value':'GUARDADO', }).css({'background-color':'#1b9b77', 'border-color':'#1b9b77', 'color':'#fff'});
                    $('#guardar').removeAttr('disabled');
                    $('.infocus').focus();
                }else{
                    // alert('False')
                    $('#guardar').attr({'value':'ERROR'}).css({'background-color':'red', 'border-color':'red', 'color':'#fff'});
                    $('#guardar').removeAttr('disabled');
                }
                console.log(resp);
            },
            error: function (jqXHR,stado,error){
                console.log(estado);
                console.log(error);
            }
        });
    });
});

//Confirmar Eliminar
$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    var id =  $(this).find('.btn-ok').attr('href');
});

//Alerta Succes
$(function(){
    setTimeout(function() {
        $(".bb-alert").fadeOut(1500);
    },2500);
});

//Poner fecha Hoy
$('[for="date-picker"]').on('click', function(e) {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = (day)+"-"+(month)+"-"+ now.getFullYear();
    $("input[name='fecha']").val(today);
    //alert(today);
});
