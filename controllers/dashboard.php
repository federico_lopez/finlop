<?php
$app->get('/dashboard', function() use($app){

    //render cuando inicia sesion
    if(!empty($_SESSION['session'])){
        $app->render('dashboard.html.twig');
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('dashboard');