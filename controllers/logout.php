<?php

$app->get('/logout', function() use($app){

	//Borro las cookies de session para no dejar ningun rastro
	//Nota: Esto destruira la session y no la informacion de la session!!
	if (ini_get("session.use_cookies")) {
	    $params = session_get_cookie_params();
	    setcookie(session_name(), '', time() - 42000,
	        $params["path"], $params["domain"],
	        $params["secure"], $params["httponly"]
	    );
	}

	//Finalmente destruyo la session
	session_destroy();

	//Redirect al login
	$app->redirect($app->urlFor('login'));

})->name('logout');