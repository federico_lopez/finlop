<?php

$app->get('/cliente', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';
        $cliente = new Cliente();
        $clientes = $cliente->vertodos();

        $app->render('clientes.html.twig', array(
            'clientes' => $clientes
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('cliente');

//Insertar Cliente
$app->post('/cliente', function() use($app){

        require_once 'models/Cliente.php';
        $cliente = new Cliente();
        $request = $app->request;
        $cliente->setNombre(strtoupper($request->post('name')));
        $cliente->setRuc($request->post('ruc'));

    if(!empty($cliente->getNombre())) {
        $cliente->insertarCliente();
        echo "true";
    }else{
        echo "false";
    }
})->name('new-cliente');

//Ir a Pag Editar
$app->get('/update/cliente/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';
        $cliente = new Cliente();

        $cliente->setIdcliente($id);
        $clienteResul = $cliente->selectCliente();

        $app->render('update/upcliente.html.twig', array(
            'cliente' => $clienteResul
        ));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('up-cliente');

//Actualiza el Cliente
$app->post('/cliente/update', function() use($app){

    require_once 'models/Cliente.php';
    $cliente = new Cliente();
    $request = $app->request;

    $cliente->setIdcliente($request->post('id'));
    $cliente->setNombre(strtoupper($request->post('nombre')));
    $cliente->setRuc(strtoupper($request->post('ruc')));

    $cliente->updateCliente();
    $app->flash('mensaje', 'Editado Correctamente');
    $app->redirect($app->urlFor('cliente'));

})->name('update-cliente');


//Elimina el cliente
$app->get('/delete/cliente/:id', function($id) use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';
        $cliente = new Cliente();

        $cliente->setIdcliente($id);
        $cliente->deleteCliente();
        $app->flash('mensaje', 'Eliminado Correctamente');
        $app->redirect($app->urlFor('cliente'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('del-cliente');