<?php

$app->get('/login', function() use($app){

    //redireccionar si hay session
    if(!empty($_SESSION['session'])){
        $app->redirect($app->urlFor('dashboard'));
    }else{
        //si no hay session dibujar la pagina de login
        $app->render('login.html.twig');
    }
})->name('login');


$app->post('/login', function() use($app){

    require_once 'models/Login.php';

    $request = $app->request;

    $login = new login($request->post('user'), $request->post('pass'));
    $login->consult();

    if($login->check() == true){
        //echo $login->pass;
        //si el login fue exitoso, ingresar redireccionando
        $app->redirect($app->urlFor('dashboard'));
    }else{
        //si no volver a la pagina de login con un mensaje flash
        $app->flash('error', 'Usuario o contraseña incorrecta');
        $app->redirect($app->urlFor('login'));
    }
});