<?php

$app->get('/categoria', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Categoria.php';
        $cat = new Categoria();
        $categorias = $cat->vertodos();

        $app->render('categoria.html.twig', array(
            'categorias' => $categorias
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('categoria');

//Insertar Categoria
$app->post('/categoria', function() use($app){

    require_once 'models/Categoria.php';
    $cat = new Categoria();

    $request = $app->request;
    $cat->setDescripcion(strtoupper($request->post('name')));

    if(!empty($cat->getDescripcion())) {
        $cat->insertarCategoria();
        echo "true";
    }else{
        echo "false";
    }

})->name('new-categoria');

$app->get('/update/categoria/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Categoria.php';
        $cat = new Categoria();

        $cat->setIdcategoria($id);
        $categoria = $cat->selectCategoria();

        $app->render('update/upcategoria.html.twig', array(
            'categorias' => $categoria
        ));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('up-categoria');

$app->post('/categoria/update', function() use($app){

    require_once 'models/Categoria.php';
    $cat = new Categoria();
    $request = $app->request;

    $cat->setIdcategoria($request->post('id'));
    $cat->setDescripcion(strtoupper($request->post('categoria')));

    $cat->updateCategoria();
    $app->flash('mensaje', 'Editado Correctamente');
    $app->redirect($app->urlFor('categoria'));

})->name('update-categoria');

//Elimina el Categoria
$app->get('/delete/categoria/:id', function($id) use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Categoria.php';
        $cat = new Categoria();
        $cat->setIdcategoria($id);
        $cat->deleteCategoria();
        $app->flash('mensaje', 'Eliminado Correctamente');
        $app->redirect($app->urlFor('categoria'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('del-categoria');