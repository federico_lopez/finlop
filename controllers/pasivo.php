<?php
/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 27/05/2016
 * Time: 07:21 PM
 */

//Pasivo Index
$app->get('/pasivo', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Pasivo.php';
        require_once 'models/Selectores.php';

        $pasivo = new Pasivo();
        $selector = new Selectores();
        $pasivoTable = $pasivo->vertodos();
        $selectCategoria = $selector->cargarCategoria();

        $app->render('pasivo.html.twig', array(
            'pas_table' => $pasivoTable, 'categorias' => $selectCategoria
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('pasivo');

//Insertar Gastos
$app->post('/pasivo', function() use($app){

    require_once 'models/Pasivo.php';
    $pasivo = new Pasivo();

    $request = $app->request;
    $pasivo->setIdcategoria($request->post('categorias'));
    $pasivo->setDescripcion($request->post('descripcion'));
    $date=date_create($request->post('fecha'));
    $pasivo->setFecha(date_format($date,"Y/m/d"));
    $pasivo->setPasivo(intval(preg_replace('/[^0-9]+/','', $request->post('salida'))));
    $insertado = "false";

    if(!empty($pasivo->getIdcategoria()) and !empty($pasivo->getDescripcion()) and !empty($pasivo->getFecha()) and !empty($pasivo->getPasivo())) {
        $insertado = $pasivo->insertarPasivo();
    }

    if($insertado == "true")
        echo "true";
    else
        echo "false";

})->name('new-pasivo');

//Eliminar Gastos
$app->get('/delete/pasivo/:id', function($id) use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Pasivo.php';
        $pasivo = new Pasivo();
        $pasivo->setIdpasivo($id);
        $pasivo->deletePasivo();
        $app->flash('mensaje', 'Eliminado Correctamente');
        $app->redirect($app->urlFor('pasivo'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('del-pasivo');


//Actualizar Gastos
$app->get('/update/pasivo/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Pasivo.php';
        require_once 'models/Selectores.php';

        $pasivo = new Pasivo();
        $selector = new Selectores();

        $selectCategoria = $selector->cargarCategoria();

        $pasivo->setIdpasivo($id);
        $pasivoArr = $pasivo->selectPasivo();

        $app->render('update/uppasivo.html.twig', array(
            'entity' => $pasivoArr, 'categorias' => $selectCategoria
        ));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('up-pasivo');

$app->post('/pasivo/update', function() use($app){

    require_once 'models/Pasivo.php';
    $pasivo = new Pasivo();
    $request = $app->request;

    $pasivo->setIdpasivo($request->post('id'));
    $pasivo->setIdcategoria($request->post('categorias'));
    $pasivo->setDescripcion($request->post('descripcion'));
    $date=date_create($request->post('fecha'));
    $pasivo->setFecha(date_format($date,"Y/m/d"));
    $pasivo->setPasivo(intval(preg_replace('/[^0-9]+/','', $request->post('gasto'))));
    $pasivo->updatePasivo();

    $app->flash('mensaje', "Editado Correctamente");
    $app->redirect($app->urlFor('pasivo'));

})->name('update-pasivo');