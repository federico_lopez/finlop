<?php
/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 30/04/2016
 * Time: 12:58 PM
 */

//Deuda Index
$app->get('/deudas', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Deudas.php';
        require_once 'models/Selectores.php';

        $deuda = new Deudas();
        $selector = new Selectores();
        $deudaTable = $deuda->vertodos();
        $selectCategoria = $selector->cargarCategoria();

        $app->render('deuda.html.twig', array(
            'deuda_table' => $deudaTable, 'categorias' => $selectCategoria
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('deudas');


//Insert Deuda
$app->post('/deuda', function() use($app){

    require_once 'models/Deudas.php';
    $deuda = new Deudas();

    $request = $app->request;
    $deuda->setIdcategoria($request->post('categorias'));
    $deuda->setDescripcion($request->post('descripcion'));
    $date=date_create($request->post('fecha'));
    $deuda->setFecha(date_format($date,"Y/m/d"));
    $deuda->setDeuda(intval(preg_replace('/[^0-9]+/','', $request->post('deuda'))));
    $insertado = "false";

    if(!empty($deuda->getIdcategoria()) and !empty($deuda->getDescripcion()) and !empty($deuda->getDeuda()) and !empty($deuda->getFecha())) {
        $insertado = $deuda->insertarDeuda();
    }

    if($insertado == "true")
        echo "true";
    else
        echo $insertado;

})->name('new-deuda');

//pagar deuda
$app->get('/deuda/pagar/:id', function($id) use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Deudas.php';

        $deuda = new Deudas();

        $deuda->setIddeudas($id);
        $app->flash('mensaje', $deuda->getIddeudas());
        $app->redirect($app->urlFor('deudas'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('pagar-deuda');