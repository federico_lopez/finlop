<?php
/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 30/04/2016
 * Time: 12:58 PM
 */


$app->get('/activo', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Activo.php';
        require_once 'models/Selectores.php';

        $activo = new Activo();
        $selector = new Selectores();
        $activoAr = $activo->vertodos();
        $selectCategoria = $selector->cargarCategoria();
        $selectClientes = $selector->cargarClientes();

        $app->render('activo.html.twig', array(
            'activos' => $activoAr, 'categorias' => $selectCategoria, 'clientes' => $selectClientes
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('activo');

$app->post('/activo', function() use($app){

    require_once 'models/Activo.php';
    $activo = new Activo();

    $request = $app->request;
    $activo->setIdcliente($request->post('clientes'));
    $activo->setIdcategoria($request->post('categorias'));
    $activo->setDescripcion($request->post('descripcion'));
    $date=date_create($request->post('fecha'));
    $activo->setFecha(date_format($date,"Y/m/d"));
    $activo->setIngreso(intval(preg_replace('/[^0-9]+/','', $request->post('importe'))));
    $insertado = "false";

    if(!empty($activo->getIdcliente()) and !empty($activo->getIdcategoria()) and !empty($activo->getDescripcion()) and !empty($activo->getIngreso()) and !empty($activo->getFecha())) {
        $insertado = $activo->insertarActivo();
    }

   if($insertado == "true")
        echo "true";
    else
        echo "false";
    //$app->redirect($app->urlFor('activo'));

})->name('new-activo');

$app->get('/update/activo/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Activo.php';
        require_once 'models/Selectores.php';

        $activo = new Activo();
        $selector = new Selectores();

        $selectCategoria = $selector->cargarCategoria();
        $selectClientes = $selector->cargarClientes();

        $activo->setIdactivo($id);
        $activoAr = $activo->selectActivo();

        $app->render('update/upactivo.html.twig', array(
            'activos' => $activoAr, 'categorias' => $selectCategoria, 'clientes' => $selectClientes
        ));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('up-activo');

$app->post('/activo/update', function() use($app){

    require_once 'models/Activo.php';
    $activo = new Activo();
    $request = $app->request;

    $activo->setIdactivo($request->post('id'));
    $activo->setIdcliente($request->post('clientes'));
    $activo->setIdcategoria($request->post('categorias'));
    $activo->setDescripcion($request->post('descripcion'));
    $date=date_create($request->post('fecha'));
    $activo->setFecha(date_format($date,"Y/m/d"));
    $activo->setIngreso(intval(preg_replace('/[^0-9]+/','', $request->post('importe'))));

    $activo->updateActivo();
    $app->flash('mensaje', 'Editado Correctamente');
    $app->redirect($app->urlFor('activo'));

})->name('update-activo');

//Elimina el activo
$app->get('/delete/activo/:id', function($id) use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Activo.php';
        $activo = new Activo();
        $activo->setIdactivo($id);
        $activo->deleteActivo();
        $app->flash('mensaje', 'Eliminado Correctamente');
        $app->redirect($app->urlFor('activo'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('del-activo');