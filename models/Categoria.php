<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 23/2/2016
 * Time: 3:35 PM
 */
class Categoria{

    private $idcategoria;
    private $descripcion;

    /**
     * @return mixed
     */
    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    /**
     * @param mixed $idcategoria
     */
    public function setIdcategoria($idcategoria)
    {
        $this->idcategoria = $idcategoria;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }


    public function vertodos(){
        $conexion = new Conexion();
        $query = $conexion->prepare("select * from categoria");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertarCategoria(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO categoria (descripcion) VALUES (:descripcion)");
        $query->bindParam("descripcion", $this->descripcion);
        //$query->execute(array('descripcion' => $this->descripcion ));
        $query->execute();
        //$result = $query->fetch(PDO::FETCH_ASSOC);
        //return $result;
        $conexion = null;
    }

    public function updateCategoria(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE categoria SET descripcion= :descripcion WHERE idcategoria= :id;");
        $query->bindParam("descripcion", $this->getDescripcion());
        $query->bindParam("id", $this->getIdcategoria());
        //$query->execute(array('descripcion' => $this->descripcion ));
        $query->execute();
        $conexion = null;
    }

    public function deleteCategoria(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM categoria WHERE idcategoria= :id;");
        $query->bindParam("id", $this->getIdcategoria());
        $query->execute();
        $conexion = null;
    }

    public function selectCategoria(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idcategoria, descripcion FROM categoria WHERE idcategoria = :id;");
        $query->bindParam("id", $this->idcategoria);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

}