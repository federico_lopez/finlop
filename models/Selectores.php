<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 03/05/2016
 * Time: 01:12 PM
 */
class Selectores
{
    public function cargarCategoria(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idcategoria, descripcion FROM categoria order by descripcion asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function cargarClientes(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idcliente, nombre FROM clientes order by nombre asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }


}