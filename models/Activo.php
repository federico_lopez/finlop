<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 30/04/2016
 * Time: 12:59 PM
 */
class Activo{

    private $idactivo;
    private $descripcion;
    private $ingreso;
    private $fecha;
    private $idcategoria;
    private $idcliente;


    /**
     * @return mixed
     */
    public function getIdactivo()
    {
        return $this->idactivo;
    }

    /**
     * @param mixed $idactivo
     */
    public function setIdactivo($idactivo)
    {
        $this->idactivo = $idactivo;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getIngreso()
    {
        return $this->ingreso;
    }

    /**
     * @param mixed $ingreso
     */
    public function setIngreso($ingreso)
    {
        $this->ingreso = $ingreso;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    /**
     * @param mixed $idcategoria
     */
    public function setIdcategoria($idcategoria)
    {
        $this->idcategoria = $idcategoria;
    }

    /**
     * @return mixed
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }

    /**
     * @param mixed $idcliente
     */
    public function setIdcliente($idcliente)
    {
        $this->idcliente = $idcliente;
    }


    public function vertodos(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT a.idactivo, a.descripcion, format(a.ingreso,0) ingreso,  DATE_FORMAT(a.fecha,'%d-%m-%Y') fecha, cat.descripcion categoria, c.nombre
                                    FROM activo a, clientes c, categoria cat
                                    where a.idcliente = c.idcliente and a.idcategoria = cat.idcategoria
                                    order by a.idactivo DESC;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }


    public function insertarActivo(){
        try{
            $conexion = new Conexion();
            $query = $conexion->prepare("INSERT INTO activo (descripcion,ingreso,fecha,idcategoria,idcliente) VALUES (:descripcion, :ingreso, :fecha, :idcategoria, :idcliente)");
            $query->bindParam("descripcion", $this->getDescripcion());
            $query->bindParam("ingreso", $this->getIngreso());
            $query->bindParam("fecha", $this->getFecha());
            $query->bindParam("idcategoria", $this->getIdcategoria());
            $query->bindParam("idcliente", $this->getIdcliente());
            $query->execute();
            $conexion = null;
            return "true";
        }catch (mysqli_sql_exception $ex){
            return "false";
        }
    }

    public function selectActivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idactivo, descripcion,  format(ingreso,0) ingreso, DATE_FORMAT(fecha,'%d-%m-%Y') fecha, idcategoria, idcliente FROM activo WHERE idactivo = :id;");
        $query->bindParam("id", $this->getIdactivo());
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function updateActivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE activo SET descripcion = :descripcion, ingreso = :ingreso, fecha = :fecha, idcategoria = :idcategoria, idcliente = :idcliente
                                    WHERE idactivo = :idactivo;");
        $query->bindParam("descripcion", $this->getDescripcion());
        $query->bindParam("ingreso", $this->getIngreso());
        $query->bindParam("fecha", $this->getFecha());
        $query->bindParam("idcategoria", $this->getIdcategoria());
        $query->bindParam("idcliente", $this->getIdcliente());
        $query->bindParam("idactivo", $this->getIdactivo());
        //$query->execute(array('descripcion' => $this->descripcion ));
        $query->execute();
        $conexion = null;
    }

    public function deleteActivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM activo WHERE idactivo= :id;");
        $query->bindParam("id", $this->getIdactivo());
        $query->execute();
        $conexion = null;
    }


}