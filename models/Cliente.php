<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 23/2/2016
 * Time: 3:35 PM
 */
class Cliente{

    private $idcliente;
    private $nombre;
    private $ruc;
    private $exeption;

    /**
     * @return mixed
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }

    /**
     * @param mixed $idcliente
     */
    public function setIdcliente($idcliente)
    {
        $this->idcliente = $idcliente;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * @param mixed $ruc
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    }




    public function vertodos(){
        $conexion = new Conexion();
        $query = $conexion->prepare("select * from clientes");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertarCliente(){
        try{
            $conexion = new Conexion();
            $query = $conexion->prepare("INSERT INTO clientes (nombre,ruc) VALUES (:nombre, :ruc)");
            $query->bindParam("nombre", $this->getNombre());
            $query->bindParam("ruc", $this->getRuc());
            $query->execute();
            $conexion = null;
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }

    }

    public function updateCliente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE clientes SET nombre=:nombre, ruc=:ruc WHERE idcliente=:id;");
        $query->bindParam("nombre", $this->getNombre());
        $query->bindParam("ruc", $this->getRuc());
        $query->bindParam("id", $this->getIdcliente());
        //$query->execute(array('descripcion' => $this->descripcion ));
        $query->execute();
        $conexion = null;
    }

    public function deleteCliente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM clientes WHERE idcliente= :id;");
        $query->bindParam("id", $this->getIdcliente());
        $query->execute();
        $conexion = null;
    }

    public function selectCliente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idcliente, nombre, ruc FROM clientes WHERE idcliente = :id;");
        $query->bindParam("id", $this->getIdcliente());
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

}