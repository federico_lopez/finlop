<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 27/05/2016
 * Time: 07:22 PM
 */
class Pasivo
{
    private $idpasivo;
    private $descripcion;
    private $pasivo;
    private $fecha;
    private $idcategoria;

    /**
     * @return mixed
     */
    public function getIdpasivo()
    {
        return $this->idpasivo;
    }

    /**
     * @param mixed $idpasivo
     */
    public function setIdpasivo($idpasivo)
    {
        $this->idpasivo = $idpasivo;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getPasivo()
    {
        return $this->pasivo;
    }

    /**
     * @param mixed $pasivo
     */
    public function setPasivo($pasivo)
    {
        $this->pasivo = $pasivo;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    /**
     * @param mixed $idcategoria
     */
    public function setIdcategoria($idcategoria)
    {
        $this->idcategoria = $idcategoria;
    }



    //Metodos BD
    public function vertodos(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT p.idpasivo, DATE_FORMAT(p.fecha,'%d-%m-%Y') fecha, c.descripcion categoria, p.descripcion, format(p.pasivo,0) salida
                    FROM pasivo p, categoria c
                    where p.idcategoria = c.idcategoria
                    order by p.idpasivo DESC;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertarPasivo(){
        try{
            $conexion = new Conexion();
            $query = $conexion->prepare("INSERT INTO pasivo (descripcion,pasivo,fecha,idcategoria) VALUES (:descripcion, :pasivo, :fecha, :idcategoria)");
            $query->bindParam("descripcion", $this->getDescripcion());
            $query->bindParam("pasivo", $this->getPasivo());
            $query->bindParam("fecha", $this->getFecha());
            $query->bindParam("idcategoria", $this->getIdcategoria());
            $query->execute();
            $conexion = null;
            return "true";
        }catch (mysqli_sql_exception $ex){
            return "false";
        }
    }

    public function deletePasivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM pasivo WHERE idpasivo= :id;");
        $query->bindParam("id", $this->getIdpasivo());
        $query->execute();
        $conexion = null;
    }

    public function selectPasivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idpasivo, descripcion,  format(pasivo,0) gasto, DATE_FORMAT(fecha,'%d-%m-%Y') fecha, idcategoria FROM pasivo WHERE idpasivo = :id;");
        $query->execute(array(':id' => $this->getIdpasivo()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function updatePasivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE pasivo SET descripcion = :descripcion, pasivo = :gasto, fecha = :fecha, idcategoria = :idcategoria WHERE idpasivo = :id;");
        $query->bindParam("descripcion", $this->getDescripcion());
        $query->bindParam("gasto", $this->getPasivo());
        $query->bindParam("fecha", $this->getFecha());
        $query->bindParam("idcategoria", $this->getIdcategoria());
        $query->bindParam("id", $this->getIdpasivo());
        $query->execute();
        $conexion = null;
    }






}