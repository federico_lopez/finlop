<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 08/06/2016
 * Time: 09:50 PM
 */
class Deudas
{
    private $iddeudas;
    private $descripcion;
    private $deuda;
    private $fecha;
    private $idcategoria;


    /**
     * @return mixed
     */
    public function getIddeudas()
    {
        return $this->iddeudas;
    }

    /**
     * @param mixed $iddeudas
     */
    public function setIddeudas($iddeudas)
    {
        $this->iddeudas = $iddeudas;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getDeuda()
    {
        return $this->deuda;
    }

    /**
     * @param mixed $deuda
     */
    public function setDeuda($deuda)
    {
        $this->deuda = $deuda;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    /**
     * @param mixed $idcategoria
     */
    public function setIdcategoria($idcategoria)
    {
        $this->idcategoria = $idcategoria;
    }

    public function verTodos(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT d.iddeudas, DATE_FORMAT(d.fecha,'%d-%m-%Y') fecha, format(d.deuda,0) deuda, d.descripcion, c.descripcion categoria
                                     FROM deudas d, categoria c
                                     WHERE d.idcategoria = c.idcategoria and d.pagado='0'
                                     ORDER BY d.fecha asc ");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertarDeuda()
    {
        try {
            $conexion = new Conexion();
            $query = $conexion->prepare("INSERT INTO deudas (descripcion,deuda,fecha,idcategoria) VALUES (:descripcion, :deuda, :fecha, :idcategoria)");
            $query->bindParam("descripcion", $this->getDescripcion());
            $query->bindParam("deuda", $this->getDeuda());
            $query->bindParam("fecha", $this->getFecha());
            $query->bindParam("idcategoria", $this->getIdcategoria());
            $query->execute();
            return "true";
        } catch (PDOException $e) {
            return '{"error":{"text":' . $e->getMessage() . '}}';
        }
        //return "true";
    }


}