<?php
class login{

    public $user;
    public $pass;
    public $result;

    public function __construct($post_user, $post_pass){
        $this->user = $post_user;
        $this->pass = $post_pass;
    }


    public function consult(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT usuario FROM user WHERE usuario = :user AND contrasena = :pass");
        $query->execute(array(':user' => $this->user, ':pass' => $this->pass));
        $this->result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
    }


    public function check(){
        if(!empty($this->result)){
            $_SESSION['session'] = $this->result;
            return true;
        }else{
            return false;
        }
    }

}
