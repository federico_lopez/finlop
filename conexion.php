<?php

class Conexion extends PDO {
	private $bd = 'mysql';
	private $host = 'localhost';
	//private $host = '192.168.0.7';
	private $name = 'finper';
	private $user = 'root';
	private $pass = '123456789';

	public function __construct() {
		//Sobreescribo el método constructor de la clase PDO.
		try{
			parent::__construct($this->bd.':host='.$this->host.';dbname='.$this->name, $this->user, $this->pass);
		}catch(PDOException $e){
			echo 'PDO exception: ' . $e->getMessage();
			exit;
		}
	}
}
